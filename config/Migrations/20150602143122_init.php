<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration 
{
  public function change()
  {
    $ratings = $this->table( 'ratings');
    $ratings 
      ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'foreign_key', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'model', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'session_id', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'value', 'float', ['precision' => 8, 'scale' => 4, 'null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'created', 'datetime', ['null' => true, 'default' => null])
      ->addColumn( 'modified', 'datetime', ['null' => true, 'default' => null])
      ->addIndex( ['user_id', 'foreign_key', 'model'])
      ->addIndex( ['session_id', 'foreign_key', 'model'])
      ->addIndex( ['foreign_key', 'model'])
      ->create();
  }


}
