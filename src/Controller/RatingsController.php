<?php
namespace Ratings\Controller;

use Ratings\Controller\AppController;

/**
 * Ratings Controller
 *
 * @property \Ratings\Model\Table\RatingsTable $Ratings
 *
 * @method \Ratings\Model\Entity\Rating[] paginate($object = null, array $settings = [])
 */
class RatingsController extends AppController
{
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function add()
  {
    $this->request->allowMethod(['post']);
    $model = $this->loadModel( $this->request->getData( 'model'));

    $result = $model->saveRating(
      $this->request->getData( 'id'),
      $this->request->getData( 'value'),
      $this->request->getSession()->id(),
      $this->Auth->user( 'id')
    );

    if( !array_key_exists( 'error', $result))
    {
      $content = $model
        ->find()
        ->where([
          $model->alias() . '.'. $model->getPrimaryKey() => $this->request->getData( 'id') 
        ])
        ->select( 'rating_avg')
        ->first();
        
      $view = new \Cake\View\View();
      $stars = $view->loadHelper( 'Ratings.Rating')->ratingImage( $content->rating_avg);
      
      $this->set([
        'success' => __d( 'app', '¡Gracias por tu valoración!'),
        'stars' => $stars
      ]);
    }
    else
    {
      $this->set([
        'error' => $result ['error']
      ]);
    }

    $this->set( '_serialize', true);
  }
}
