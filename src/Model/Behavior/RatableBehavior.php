<?php
/**
 * Copyright 2010, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
namespace Ratings\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Utility\Hash;

/**
 * CakePHP Ratings Plugin
 *
 * Ratable behavior
 *
 * @package 	ratings
 * @subpackage 	ratings.models.behaviors
 */
class RatableBehavior extends Behavior {

	/**
	 * Default settings
	 *
	 * modelClass		- must be set in the case of a plugin model to make the behavior work with plugin models like 'Plugin.Model'
	 * rateClass		- name of the rate class model
	 * foreignKey		- foreign key field
	 * saveToField		- boolean, true if the calculated result should be saved in the rated model
	 * field 			- name of the field that is updated with the calculated rating
	 * fieldSummary		- optional cache field that will store summary of all ratings that allow to implement quick rating calculation
	 * fieldCounter		- optional cache field that will store count of all ratings that allow to implement quick rating calculation
	 * calculation		- 'average' or 'sum', default is average
	 * update			- boolean flag, that define permission to rerate(change previous rating)
	 * modelValidate	- validate the model before save, default is false
	 * modelCallbacks	- run model callbacks when the rating is saved to the model, default is false
	 * allowedValues	- @todo
	 *
	 * @var array
	 */
	protected $_defaultConfig = [
		'modelClass' => null,
		'rateClass' => 'Ratings.Ratings',
		'foreignKey' => 'foreign_key',
		'field' => 'rating',
		'fieldSummary' => 'rating_sum',
		'fieldCounter' => 'rating_count',
		'calculation' => 'average',
		'saveToField' => true,
		'countRates' => false,
		'update' => false,
		'modelValidate' => false,
		'modelCallbacks' => false,
		'allowedValues' => []
	];

	/**
	 * Rating modes
	 *
	 * @var array
	 */
	public $modes = [
		'average' => 'avg',
		'sum' => 'sum',
	];

	/**
	 * Setup
	 *
	 * @param array $config Config
	 * @return void
	 */
	public function initialize(array $config) {
		if (empty($this->_config['modelClass'])) {
			$this->_config['modelClass'] = $this->_table->alias();
		}

		$this->_table->hasMany('Ratings', [
				'className' => $this->_config['rateClass'],
				'foreignKey' => $this->_config['foreignKey'],
				'unique' => true,
				'dependent' => true,
			]
		);

		$this->_table->Ratings->belongsTo($this->_config['modelClass'],
			[
				'className' => $this->_config['modelClass'],
				'foreignKey' => 'foreign_key',
				'counterCache' => $this->_config['countRates']
			]
		);
		//die(debug($this->_table));
	}

	/**
	 * Saves a new rating
	 *
	 * @param string $foreignKey
	 * @param string|int $userId
	 * @param int $value
	 * @return bool|float Boolean or calculated sum
	 */
	public function saveRating( $foreignKey, $value, $sessionId, $userId) 
	{
		$exists = $this->isRatedBy( $foreignKey, $sessionId, $userId);

		if( $exists)
		{
			return [
				'error' => __d( 'app', 'Ya has valorado este contenido')
			];
		}

		$data = [
			'foreign_key' => $foreignKey,
			'session_id' => $sessionId,
			'user_id' => $userId,
			'value' => $value,
			'model' => $this->_table->alias(),
		];

		$entity = $this->_table->Ratings->newEntity( $data);

		$this->_table->Ratings->save( $entity);
		$this->calculateRating( $foreignKey);		
		return $entity;
	}

	/**
	 * Remove exists rating
	 *
	 * @param string $foreignKey
	 * @param string $userId
	 * @return bool|float Boolean or calculated sum
	 */
	public function removeRating($foreignKey, $userId) 
	{
		
	}

	

	

	/**
	 * Calculates the rating
	 *
	 * This method does always a calculation of the the values based on SQL AVG()
	 * and SUM(). Please note that this is relatively slow compared to incrementing
	 * the values, see Ratable::incrementRating()
	 *
	 * @param string $foreignKey
	 * @throws \Exception
	 */
	public function calculateRating( $foreignKey) 
	{
		$result = $this->_table->Ratings->find()
			->where([
				'Ratings.foreign_key' => $foreignKey,
				'Ratings.model' => $this->_table->alias()
			])
			->select( function( $q){
				return [
					'sum' => $q->func()->sum( 'Ratings.value'),
					'count' => $q->func()->count('*'),
				];
			})
			->first();
			;

			$this->_table->query()->update()
			->set([
				'rating_avg' => round( ($result->sum / $result->count), 2),
				'rating_sum' => $result->sum,
				'rating_count' => $result->count,
			])
			->where([
				'id' => $foreignKey
			])
			->execute();
	}

	/**
	 * Method to check if an entry is rated by a certain user
	 *
	 * @param int|string|array $foreignKey foreign key as uuid or int or array of foreign keys
	 * @param int|string $sessionId
	 * @param int|string $userId
	 * @return mixed Array of related foreignKeys when querying for multiple entries, entry or false otherwise
	 */
	public function isRatedBy( $foreignKey, $sessionId, $userId = null) 
	{
		$entry =  $this->_table->Ratings->find()
			->where([
				'Ratings.foreign_key IN' => $foreignKey,
				'Ratings.model' => $this->_table->alias()
			]);
		
		if( !empty( $userId))
		{
			$entry->where([
				'Ratings.user_id' => $userId
			]);
		}
		else
		{
			$entry->where([
				'Ratings.session_id' => $sessionId
			]);
		}

  	$entry = $entry->first();

		if( $entry) 
		{
			$entry = $entry->toArray();
		}

		if( empty( $entry)) 
		{
			return false;
		}

		return $entry;
	}

	/**
	 * afterRate callback to the model
	 *
	 * @param array
	 * @return void
	 */
	public function afterRateCallback($data = []) {
		if (method_exists($this->_table, 'afterRate')) {
			$this->_table->afterRate($data);
		}
	}

	/**
	 * beforeRate callback to the model
	 *
	 * @param array $data
	 * @return void
	 */
	public function beforeRateCallback(array $data = []) {
		if (method_exists($this->_table, 'beforeRate')) {
			$this->_table->beforeRate($data);
		}
	}



	/**
	 * Caches the sum of the different ratings for each of them
	 *
	 * For example a rating of 1 will increase the value in the field "rating_1" by 1,
	 * a rating of 2 will increase "rating_2" by one...
	 *
	 * @param array $data Data passed to afterRate() or similar structure
	 * @return bool True on success
	 * @throws \Exception
	 */
	public function cacheRatingStatistics($data = []) {
		extract($data);

		if (!$result) {
			return false;
		}

		if ($type === 'removeRating') {
			$value = $oldRating['value'];
		}

		if (!$this->_table->hasField($this->_fieldName(round($value, 0)))) {
			return false;
		}

		if (is_array($foreignKey)) {
			throw new \Exception('Array not supported for $foreignKey here');
		}

		$data = $this->_table->find('all', [
			'conditions' => [
				$this->_table->alias() . '.' . $this->_table->primaryKey() => $foreignKey],
		])->first();

		if (($update || $type === 'removeRating') && !empty($oldRating)) {
			$oldId = round($oldRating['value']);
			$data[$this->_fieldName($oldId)] -= 1;
		}

		if ($type === 'saveRating') {
			$newId = round($value);
			$data[$this->_fieldName($newId)] += 1;
		}

		return $this->_table->save($data, [
			'callbacks' => $this->_config['modelCallbacks']
		]);
	}

	/**
	 * Return field name for cache value
	 *
	 * @param string $value
	 * @param string $prefix
	 * @return string
	 */
	protected function _fieldName($value, $prefix = 'rating_') {
		$postfix = $value;
		if ($value < 0) {
			$postfix = 'neg' . abs($value);
		}
		return $prefix . $postfix;
	}

}
