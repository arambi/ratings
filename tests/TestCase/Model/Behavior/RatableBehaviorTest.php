<?php
/**
 * Copyright 2010, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
namespace Ratings\Test\TestCase\Model\Behavior;

use App\Model\Model;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * CakePHP Ratings Plugin
 *
 * Ratable behavior tests
 *
 * @package 	ratings
 * @subpackage 	ratings.tests.cases.behaviors
 */
class RatableBehaviorTest extends TestCase {

	/**
	 * Holds the instance of the model
	 *
	 * @var mixed
	 */
	public $Articles = null;

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = [
		'plugin.ratings.ratings',
		'plugin.ratings.articles',
		'plugin.ratings.posts',
		'plugin.ratings.users'
	];

	/**
	 * startTest
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->Articles = TableRegistry::get('Articles');
		$this->Posts = TableRegistry::get('Posts');

		//$this->loadFixtures('Rating');
	}

	/**
	 * endTest
	 *
	 * @return void
	 */
	public function tearDown() {
		parent::tearDown();
		unset($this->Articles);
		unset($this->Posts);
		TableRegistry::clear();
	}

	public function testSaveRating()
	{
		$this->Articles->addBehavior('Ratings.Ratable', []);
		$article = $this->Articles->find()->first();
		$entity = $this->Articles->saveRating( $article->id, 4, 'abc', null);
		$this->assertEquals( $article->id, $entity->foreign_key);
		$this->assertEquals( 'Articles', $entity->model);
		$this->assertEquals( 'abc', $entity->session_id);
		$this->assertEquals( 4, $entity->value);
		
		$article = $this->Articles->find()->first();
		$entity = $this->Articles->saveRating( $article->id, 4, 'abc', null);
		$this->assertEquals( 'Ya has valorado este contenido', $entity ['error']);
	}


	public function testSaveAverage()
	{
		$this->Articles->addBehavior('Ratings.Ratable', []);
		$article = $this->Articles->find()->first();
		$total = 0;

		for( $i = 1; $i <= 10; $i++)
		{
			$value = rand( 1, 5);
			$total += $value;
			$entity = $this->Articles->saveRating( $article->id, $value, uniqid( rand( 0, 9999999)), null);
		}

		$avg = round( ($total / 10), 2);
		$article = $this->Articles->find()->first();
		$this->assertEquals( $avg, $article->rating_avg);
		$this->assertEquals( $total, $article->rating_sum);
		$this->assertEquals( 10, $article->rating_count);
	}
}
