<?php
/**
 * Copyright 2010, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
namespace Ratings\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CakePHP Ratings Plugin
 *
 * Article fixture
 *
 * @package 	ratings
 * @subpackage 	ratings.tests.fixtures
 */
class ArticlesFixture extends TestFixture {

	/**
	 * fields property
	 *
	 * @var array
	 * @access public
	 */
	public $fields = [
		'id' => ['type' => 'integer'],
		'user_id' => ['type' => 'integer', 'null' => false, 'length' => 10],
		'title' => ['type' => 'string', 'null' => false],
		'rating_avg' => ['type' => 'float', 'length' => 20, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => '0.000000', 'comment' => ''],
		'rating_count' => ['type' => 'integer', 'null' => false, 'default' => '0'],
		'rating_sum' => ['type' => 'integer', 'null' => false, 'default' => '0'],
		'_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]]
	];

	/**
	 * records property
	 *
	 * @var array
	 * @access public
	 */
	public $records = [
		[
			'id' => 1,
			'user_id' => 0,
			'title' => 'First Article',
			'rating_avg' => 0,
			'rating_count' => 0,
		],
	];

}
